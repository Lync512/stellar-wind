# Stellar Wind

Repository for Project Stellar wind. An ESP32 based WiFi & Bluetooth scanner I designed as a wireless security auditing tool.

While this isn't a projcet I plan to get prouduced, I still wanted to release it as open source in hopes someone can find it useful. 
This project is licensed under a CERN-OHL-P v2 license. You are free to copy, modify, re-upload, improve and prouduce this. 


Additional things of note, while this primarly uses the WiFi and Bluetooth functions of the ESP32, all unused GPIO pins have been wired to a pinheader so that they can be utilized as well. 

I had fun working on this projcet. And I look foward to working on more in the future. 

To design this I used the KiCAD suite. 


![Stellar_wind](Stream_1_RTX.png)
